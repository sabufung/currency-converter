import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride;

void main() {
  debugDefaultTargetPlatformOverride =
      TargetPlatform.fuchsia; // for desktop embedder
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Currency Converter'),
    );
  }
}

// Code layout in column
class VerticalLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Column(
        children: <Widget>[
          ConverterOption(),
          keyboard,
        ],
      ),
    );
  }
}

// Code layout in row
class HorizontalLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Row(
        children: <Widget>[
          ConverterOption(),
          keyboard,
        ],
      ),
    );
  }
}

class ConverterOption extends StatefulWidget {
  ConverterOption({Key key}) : super(key: key);

  _ConverterOptionState createState() => _ConverterOptionState();
}

class _ConverterOptionState extends State<ConverterOption> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
          flex: 6,
          child: Column(
            children: <Widget>[
              Expanded(
                  child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.grey))),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: GestureDetector(
                        onTap: () {
                          print("onTap called.");
                        },
                        child: Container(
                          decoration: new BoxDecoration(
                              color: Colors.lightBlue.withOpacity(0.2),
                              border: Border(
                                  right: BorderSide(
                                      width: 0.5, color: Colors.grey))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Image.asset("assets/images/russia.png",
                                  height: 25),
                              Text("RUB", style: TextStyle(fontSize: 22)),
                            ],
                          ),
                          alignment: Alignment.center,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: Container(
                        color: Colors.lightBlue.withOpacity(0.2),
                        padding: EdgeInsets.all(14),
                        child: Text(
                          "23 102.83",
                          style: TextStyle(fontSize: 22),
                        ),
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ],
                ),
              )),
              Expanded(
                  child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.grey))),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                        decoration: new BoxDecoration(
                            border: Border(
                                right: BorderSide(
                                    width: 0.5, color: Colors.grey))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset(
                                "assets/images/united-states-of-america.png",
                                height: 25),
                            Text("USD", style: TextStyle(fontSize: 22)),
                          ],
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: Container(
                        padding: EdgeInsets.all(14),
                        child: Text(
                          "4 232",
                          style: TextStyle(fontSize: 22),
                        ),
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ],
                ),
              )),
              Expanded(
                  child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.grey))),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                        decoration: new BoxDecoration(
                            border: Border(
                                right: BorderSide(
                                    width: 0.5, color: Colors.grey))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset("assets/images/european-union.png",
                                height: 25),
                            Text("EUR", style: TextStyle(fontSize: 22)),
                          ],
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: Container(
                        padding: EdgeInsets.all(14),
                        child: Text(
                          "92.14",
                          style: TextStyle(fontSize: 22),
                        ),
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ],
                ),
              )),
              Expanded(
                  child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.grey))),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                        decoration: new BoxDecoration(
                            border: Border(
                                right: BorderSide(
                                    width: 0.5, color: Colors.grey))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset("assets/images/united-kingdom.png",
                                height: 25),
                            Text("GBP", style: TextStyle(fontSize: 22)),
                          ],
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: Container(
                        padding: EdgeInsets.all(14),
                        child: Text(
                          "34 030.30",
                          style: TextStyle(fontSize: 22),
                        ),
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ],
                ),
              )),
              Expanded(
                  child: Container(
                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(width: 0.5, color: Colors.grey))),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                        decoration: new BoxDecoration(
                            border: Border(
                                right: BorderSide(
                                    width: 0.5, color: Colors.grey))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Image.asset("assets/images/canada.png", height: 25),
                            Text(
                              "CAD",
                              style: TextStyle(fontSize: 22),
                            ),
                          ],
                        ),
                        alignment: Alignment.center,
                      ),
                    ),
                    Expanded(
                      flex: 7,
                      child: Container(
                        padding: EdgeInsets.all(14),
                        child: Text(
                          "102.49",
                          style: TextStyle(fontSize: 22),
                        ),
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ],
                ),
              )),
            ],
          )),
    );
  }
}

Widget keyboard = Expanded(
    flex: 4,
    child: Row(
      children: <Widget>[
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("1",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(1),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("4",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(4),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("7",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(7),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text(".",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(11),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("2",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(2),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("5",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(5),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("8",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(8),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("0",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(0),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("3",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(3),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("6",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(6),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Text("9",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w300)),
                  onPressed: () => keyPressed(9),
                ),
              ),
              Expanded(
                child: FlatButton(
                  color: Colors.white,
                  child: Icon(
                    Icons.backspace,
                    color: Colors.blueAccent,
                  ),
                  onPressed: () => keyPressed(10),
                ),
              ),
            ],
          ),
        ),
      ],
    ));

keyPressed(int i) {
  print(i);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: LayoutBuilder(
          builder: (context, constraints) {
            if (constraints.maxWidth > 500)
              return HorizontalLayout();
            else
              return VerticalLayout();
          },
        ));
  }
}
